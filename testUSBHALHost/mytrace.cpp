#include "mytrace.h"
myTrace::myTrace():wpos(0)
{
}

void myTrace::trace(const char* func, int line)
{
    buf[wpos%TRACE_BUF_SIZE].func = func;
    buf[wpos%TRACE_BUF_SIZE].line = line;
    buf[wpos%TRACE_BUF_SIZE].value = 0;
    wpos++;
}

void myTrace::trace1(const char* func, int line, int value)
{
    buf[wpos%TRACE_BUF_SIZE].func = func;
    buf[wpos%TRACE_BUF_SIZE].line = line;
    buf[wpos%TRACE_BUF_SIZE].value = value;
    wpos++;
}

myTrace myt;

