#ifdef TEST_MAIN
#include "mbed.h"

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);

int main()
{
    pc.baud(115200);
    
    for(int n = 0; ; n++) {
        led1 = !led1;
        pc.printf("%d\n", n);
        wait_ms(500);
    }
}
#endif
