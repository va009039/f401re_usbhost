#ifdef TEST_HCD1
#include "mbed.h"
#include "mytrace.h"
#include "mydebug.h"

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);

#define USB_DBG(...) do{pc.printf("[%s@%d] ",__PRETTY_FUNCTION__,__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define USB_INFO(...) do{pc.printf(__VA_ARGS__);pc.puts("\n");}while(0);
#define USB_TEST_ASSERT(A) while(!(A)){pc.printf("\n\n%s@%d %s ASSERT!\n\n",__PRETTY_FUNCTION__,__LINE__,#A);exit(1);};
#define USB_DBG_HEX(A,B) debug_hex<RawSerial>(pc, A,B)
#define USB_TRACE_VIEW() myt.view<RawSerial>(pc)

// USBHALHost.cpp
extern __IO bool attach_done;
extern __IO bool token_done;
extern __IO HCD_URBStateTypeDef g_urb_state;
extern void delay_ms(uint32_t t);
extern HCD_HandleTypeDef hhcd_USB_OTG_FS;

int dev_address = 0;
int dev_speed = HCD_SPEED_LOW;
int dev_mps = 8;
int intr_mps = 4;

void ctrl_init()
{
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, 0, 0x00, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, 1, 0x80, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
}

void ctrl_recv(const uint8_t* setup, uint8_t* buf, int size)
{
    // SETUP
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 0, 0, EP_TYPE_CTRL, 0, (uint8_t*)setup, 8, 0);
    while(g_urb_state == URB_IDLE);
    USB_TRACE1(g_urb_state);
    if (g_urb_state != URB_DONE) {
        return;
    }
    // DATA_IN
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 1, 1, EP_TYPE_CTRL, 1, buf, size, 0);
    while(g_urb_state == URB_IDLE);
    USB_TRACE1(g_urb_state);
    if (g_urb_state != URB_DONE) {
        return;
    }
    // DATA_OUT, status stage
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 0, 0, EP_TYPE_CTRL, 1, NULL, 0, 0);
    while(g_urb_state == URB_IDLE);
    USB_TRACE1(g_urb_state);
}

static HAL_StatusTypeDef HAL_HCD_HC_SubmitRequest2(HCD_HandleTypeDef *hhcd,
                                            uint8_t ch_num, 
                                            uint8_t direction ,
                                            uint8_t ep_type,  
                                            uint8_t token, 
                                            uint8_t* pbuff, 
                                            uint16_t length,
                                            uint8_t do_ping) 
{
    HCD_HCTypeDef* hc = &(hhcd->hc[ch_num]);
    hc->ep_is_in = direction;
    hc->ep_type  = ep_type; 

    if (hc->toggle_in == 0) {
        hc->data_pid = HC_PID_DATA0;
    } else {
        hc->data_pid = HC_PID_DATA1;
   }

  hc->xfer_buff = pbuff;
  hc->xfer_len  = length;
  hc->urb_state = URB_IDLE;
  hc->xfer_count = 0 ;
  hc->ch_num = ch_num;
  hc->state = HC_IDLE;

  return USB_HC_StartXfer(hhcd->Instance, hc, hhcd->Init.dma_enable);
}

void ctrl_token_in(uint8_t* buf, int size)
{
    const int chnum = 1;
    const int IN = 1;
    hhcd_USB_OTG_FS.hc[chnum].toggle_in = 1;
    // DATA_IN
    for(int pos = 0; pos < size; ) {
        int size2 = size - pos;
        if (size2 > dev_mps) {
            size2 = dev_mps;
        }
        g_urb_state = URB_IDLE;
#if 0
        HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, chnum, IN, EP_TYPE_CTRL, 1, buf + pos, size2, 0);
#else
        HAL_HCD_HC_SubmitRequest2(&hhcd_USB_OTG_FS, chnum, IN, EP_TYPE_CTRL, 1, buf + pos, size2, 0);
#endif
        while(g_urb_state == URB_IDLE);
        USB_TRACE1(g_urb_state);
        if (g_urb_state != URB_DONE) {
            return;
        }
        int result = HAL_HCD_HC_GetXferCount(&hhcd_USB_OTG_FS, chnum);
        USB_TRACE1(result);
        pos += result;
        if (result < dev_mps) {
            break;
        }
    }
}

void ctrl_recv2(const uint8_t* setup, uint8_t* buf, int size)
{
    // SETUP
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 0, 0, EP_TYPE_CTRL, 0, (uint8_t*)setup, 8, 0);
    while(g_urb_state == URB_IDLE);
    USB_TRACE1(g_urb_state);
    if (g_urb_state != URB_DONE) {
        return;
    }
    // DATA_IN
    ctrl_token_in(buf, size);
    USB_TRACE1(g_urb_state);
    if (g_urb_state != URB_DONE) {
        return;
    }
    // DATA_OUT, status stage
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 0, 0, EP_TYPE_CTRL, 1, NULL, 0, 0);
    while(g_urb_state == URB_IDLE);
    USB_TRACE1(g_urb_state);
}

void ctrl_send(const uint8_t* setup, uint8_t* buf = NULL, int size = 0)
{
    // SETUP
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 0, 0, EP_TYPE_CTRL, 0, (uint8_t*)setup, 8, 0);
    while(g_urb_state == URB_IDLE);
    USB_TEST_ASSERT(g_urb_state == URB_DONE);

    // DATA_IN, status stage
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, 1, 1, EP_TYPE_CTRL, 1, NULL, 0, 0);
    while(g_urb_state == URB_IDLE);
}

void intr_init()
{
    const int chnum = 2;
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, chnum, 0x81, dev_address, dev_speed, EP_TYPE_INTR, intr_mps);
}

bool intr_recvNB(uint8_t* buf, int size)
{
    const int chnum = 2;
    const int IN = 1;
    // DATA_IN
    token_done = false;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, chnum, IN, EP_TYPE_INTR, 1, buf, size, 0);
    while(!token_done);
    int s = HAL_HCD_HC_GetURBState(&hhcd_USB_OTG_FS, chnum);
    return s == URB_DONE;

#if 0
    USB_TRACE_CLEAR();
    for(int i = 0; i < 10; i++) {
        int r1 = HAL_HCD_HC_GetURBState(&hhcd_USB_OTG_FS, chnum);
        USB_TRACE1(r1);
        int r2 = HAL_HCD_HC_GetState(&hhcd_USB_OTG_FS, chnum);
        USB_TRACE1(r2);
        delay_ms(1);
    }
    USB_TRACE_VIEW(RawSerial,pc);
    while(!g_notify);
    USB_TEST_ASSERT(g_urb_state == URB_DONE);
    //USB_TEST_ASSERT(g_urb_state != URB_ERROR);
#endif
}


int main()
{
    pc.baud(115200);
    USB_DBG("%s", __FILE__);

    USB_TRACE();

    hhcd_USB_OTG_FS.Instance = USB_OTG_FS;
    hhcd_USB_OTG_FS.Init.Host_channels = 8;
    hhcd_USB_OTG_FS.Init.speed = HCD_SPEED_FULL;
    hhcd_USB_OTG_FS.Init.dma_enable = DISABLE;
    hhcd_USB_OTG_FS.Init.phy_itface = HCD_PHY_EMBEDDED;
    hhcd_USB_OTG_FS.Init.Sof_enable = DISABLE;
    hhcd_USB_OTG_FS.Init.low_power_enable = ENABLE;
    hhcd_USB_OTG_FS.Init.vbus_sensing_enable = DISABLE;
    hhcd_USB_OTG_FS.Init.use_external_vbus = DISABLE;

    HAL_HCD_Init(&hhcd_USB_OTG_FS);
    HAL_HCD_Start(&hhcd_USB_OTG_FS);

    while(!attach_done);
    USB_DBG("USB Device Attached");

    delay_ms(200);
    HAL_HCD_ResetPort(&hhcd_USB_OTG_FS);
    delay_ms(100); // Wait for 100 ms after Reset

    int root_speed = HAL_HCD_GetCurrentSpeed(&hhcd_USB_OTG_FS);
    USB_TRACE1(root_speed);
    USB_DBG("root_speed=%d", root_speed);
    USB_TEST_ASSERT(root_speed != USB_OTG_SPEED_HIGH);
    if (root_speed == USB_OTG_SPEED_LOW) {
        dev_speed = HCD_SPEED_LOW;
    } else {
        dev_speed = HCD_SPEED_FULL;
    }

#if 1
    const uint8_t GET_DESCRIPTOR = 0x06;
    uint8_t setup[8] = {0x80, GET_DESCRIPTOR, 0x00, 0x01, 0x00, 0x00, 8, 0x00};
    uint8_t buf[18];
    ctrl_init();
    ctrl_recv(setup, buf, 8);
    //USB_TRACE_VIEW(RawSerial,pc);
    USB_TRACE_CLEAR();
    USB_TEST_ASSERT(g_urb_state == URB_DONE);
    USB_DBG_HEX(buf, 8);
    dev_mps = buf[7];
    USB_DBG("dev_mps=%d", dev_mps);
#endif

    const uint8_t SET_ADDRESS = 0x05;
    const uint8_t new_dev_address = 1;
    uint8_t setup_set_address[8] = {0x00, SET_ADDRESS, new_dev_address, 0x00, 0x00, 0x00, 0x00, 0x00};
    ctrl_init();
    ctrl_send(setup_set_address);
    USB_TEST_ASSERT(g_urb_state == URB_DONE);
    dev_address = new_dev_address;
    delay_ms(100);

#if 1
    uint8_t desc[18];
    uint8_t setup_get_desc[8] = {0x80, GET_DESCRIPTOR, 0x00, 0x01, 0x00, 0x00, sizeof(desc), 0x00};
    ctrl_init();
    ctrl_recv(setup_get_desc, desc, sizeof(desc));
    USB_TEST_ASSERT(g_urb_state == URB_DONE);
    USB_DBG_HEX(desc, sizeof(desc));
#endif

#if 1
    uint8_t temp[4];
    uint8_t setup_get_desc2[8] = {0x80, GET_DESCRIPTOR, 0x00, 0x02, 0x00, 0x00, sizeof(temp), 0x00};
    ctrl_init();
    ctrl_recv(setup_get_desc2, temp, sizeof(temp));
    USB_TEST_ASSERT(g_urb_state == URB_DONE);
    USB_DBG_HEX(temp, sizeof(temp));
    int totalLength = temp[2]|temp[3]<<8;
    USB_DBG("totalLength=%d", totalLength);
    uint8_t* desc2 = new uint8_t[totalLength];
    memcpy(setup_get_desc2+6, temp+2, 2);

    USB_TRACE_CLEAR();
    ctrl_recv2(setup_get_desc2, desc2, totalLength);

    USB_TRACE_VIEW();
    USB_TEST_ASSERT(g_urb_state == URB_DONE);
    USB_DBG_HEX(desc2, totalLength);
    for(int pos = 0; pos < totalLength;) {
        int len = desc2[pos];
        USB_DBG_HEX(desc2+pos, len);
        pos += len;
    }
#endif

    const uint8_t SET_CONFIGURATION = 0x09;
    uint8_t setup_set_config[8] = {0x00, SET_CONFIGURATION, 1, 0x00, 0x00, 0x00, 0x00, 0x00};
    ctrl_init();
    ctrl_send(setup_set_config);
    USB_TEST_ASSERT(g_urb_state == URB_DONE);

#if 1
    USB_TRACE_CLEAR();
    uint8_t data[4];
    intr_init();

    for(int cnt = 0; cnt < 100;) {
        if(intr_recvNB(data, sizeof(data))) {
            USB_DBG_HEX(data, sizeof(data));
            cnt++;
            int r1 = HAL_HCD_HC_GetURBState(&hhcd_USB_OTG_FS, 2);
            USB_TRACE1(r1);
            int r2 = HAL_HCD_HC_GetState(&hhcd_USB_OTG_FS, 2);
            USB_TRACE1(r2);
        }
        delay_ms(10);
    }
    //USB_TRACE_VIEW(RawSerial, pc);

#endif
    
    while(1) {
        led1 = !led1;
        delay_ms(200);
    }
}

#endif
