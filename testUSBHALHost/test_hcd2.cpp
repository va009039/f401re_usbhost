#ifdef TEST_HCD2
#include "mbed.h"
#include "mytrace.h"
#include "mydebug.h"

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);

#define USB_DBG(...) do{pc.printf("[%s@%d] ",__PRETTY_FUNCTION__,__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define USB_INFO(...) do{pc.printf(__VA_ARGS__);pc.puts("\n");}while(0);
#define USB_TEST_ASSERT(A) while(!(A)){pc.printf("\n\n%s@%d %s ASSERT!\n\n",__PRETTY_FUNCTION__,__LINE__,#A);exit(1);};
#define USB_DBG_HEX(A,B) debug_hex<RawSerial>(pc, A,B)
#define USB_TRACE_VIEW() myt.view<RawSerial>(pc)

// USBHALHost.cpp
extern __IO bool attach_done;
extern __IO bool token_done;
extern __IO HCD_URBStateTypeDef g_urb_state;
extern void delay_ms(uint32_t t);
extern HCD_HandleTypeDef hhcd_USB_OTG_FS;

static const int CH_CTL_IN  = 0;
static const int CH_CTL_OUT = 1;
static const int CH_INT_IN  = 2;
static const int DIR_IN  = 1;
static const int DIR_OUT = 0;

int dev_address = 0;
int dev_speed = HCD_SPEED_LOW;
int dev_mps = 8;
int intr_mps = 4;

static HAL_StatusTypeDef HAL_HCD_HC_SubmitRequest2(HCD_HandleTypeDef *hhcd,
                                            uint8_t ch_num, 
                                            uint8_t direction ,
                                            uint8_t ep_type,  
                                            uint8_t token, 
                                            uint8_t* pbuff, 
                                            uint16_t length,
                                            uint8_t do_ping) 
{
    HCD_HCTypeDef* hc = &(hhcd->hc[ch_num]);
    hc->ep_is_in = direction;
    hc->ep_type  = ep_type; 

    if (hc->toggle_in == 0) {
        hc->data_pid = HC_PID_DATA0;
    } else {
        hc->data_pid = HC_PID_DATA1;
   }

  hc->xfer_buff = pbuff;
  hc->xfer_len  = length;
  hc->urb_state = URB_IDLE;
  hc->xfer_count = 0 ;
  hc->ch_num = ch_num;
  hc->state = HC_IDLE;

  return USB_HC_StartXfer(hhcd->Instance, hc, hhcd->Init.dma_enable);
}

void ctrl_token_in(uint8_t* buf, int size)
{
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, CH_CTL_IN,  0x80, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
    hhcd_USB_OTG_FS.hc[CH_CTL_IN].toggle_in = 1;
    // DATA_IN
    for(int pos = 0; pos < size; ) {
        int size2 = size - pos;
        if (size2 > dev_mps) {
            size2 = dev_mps;
        }
        g_urb_state = URB_IDLE;
        HAL_HCD_HC_SubmitRequest2(&hhcd_USB_OTG_FS, CH_CTL_IN, DIR_IN, EP_TYPE_CTRL, 1, buf + pos, size2, 0);
        while(g_urb_state == URB_IDLE);
        if (g_urb_state != URB_DONE) {
            return;
        }
        int result = HAL_HCD_HC_GetXferCount(&hhcd_USB_OTG_FS, CH_CTL_IN);
        USB_TRACE1(result);
        pos += result;
        if (result < dev_mps) {
            break;
        }
    }
}

void ctrl_recv(const uint8_t* setup, uint8_t* buf, int size)
{
    // SETUP
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, CH_CTL_OUT, 0x00, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, CH_CTL_OUT, DIR_OUT, EP_TYPE_CTRL, 0, (uint8_t*)setup, 8, 0);
    while(g_urb_state == URB_IDLE);
    if (g_urb_state != URB_DONE) {
        return;
    }
    // DATA_IN
    ctrl_token_in(buf, size);

    // DATA_OUT, status stage
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, CH_CTL_OUT, 0x00, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, CH_CTL_OUT, DIR_OUT, EP_TYPE_CTRL, 1, NULL, 0, 0);
    while(g_urb_state == URB_IDLE);
}

void ctrl_send(const uint8_t* setup, uint8_t* buf = NULL, int size = 0)
{
    // SETUP
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, CH_CTL_OUT, 0x00, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, CH_CTL_OUT, DIR_OUT, EP_TYPE_CTRL, 0, (uint8_t*)setup, 8, 0);
    while(g_urb_state == URB_IDLE);
    if (g_urb_state != URB_DONE) {
        return;
    }
    // DATA_IN, status stage
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, CH_CTL_IN,  0x80, dev_address, dev_speed, EP_TYPE_CTRL, dev_mps);
    g_urb_state = URB_IDLE;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, CH_CTL_IN, DIR_IN, EP_TYPE_CTRL, 1, NULL, 0, 0);
    while(g_urb_state == URB_IDLE);
}

bool intr_recvNB(uint8_t* buf, int size)
{
    HAL_HCD_HC_Init(&hhcd_USB_OTG_FS, CH_INT_IN, 0x81, dev_address, dev_speed, EP_TYPE_INTR, intr_mps);

    // DATA_IN
    token_done = false;
    HAL_HCD_HC_SubmitRequest(&hhcd_USB_OTG_FS, CH_INT_IN, DIR_IN, EP_TYPE_INTR, 1, buf, size, 0);
    while(!token_done);
    return g_urb_state == URB_DONE;
}

int main()
{
    pc.baud(115200);
    USB_DBG("%s", __FILE__);

    USB_TRACE();

    hhcd_USB_OTG_FS.Instance = USB_OTG_FS;
    hhcd_USB_OTG_FS.Init.Host_channels = 8;
    hhcd_USB_OTG_FS.Init.speed = HCD_SPEED_FULL;
    hhcd_USB_OTG_FS.Init.dma_enable = DISABLE;
    hhcd_USB_OTG_FS.Init.phy_itface = HCD_PHY_EMBEDDED;
    hhcd_USB_OTG_FS.Init.Sof_enable = DISABLE;
    hhcd_USB_OTG_FS.Init.low_power_enable = ENABLE;
    hhcd_USB_OTG_FS.Init.vbus_sensing_enable = DISABLE;
    hhcd_USB_OTG_FS.Init.use_external_vbus = DISABLE;

    HAL_HCD_Init(&hhcd_USB_OTG_FS);
    HAL_HCD_Start(&hhcd_USB_OTG_FS);

    while(!attach_done);
    USB_DBG("USB Device Attached");

    delay_ms(200);
    HAL_HCD_ResetPort(&hhcd_USB_OTG_FS);
    delay_ms(100); // Wait for 100 ms after Reset

    int root_speed = HAL_HCD_GetCurrentSpeed(&hhcd_USB_OTG_FS);
    USB_TRACE1(root_speed);
    USB_DBG("root_speed=%d", root_speed);
    USB_TEST_ASSERT(root_speed != USB_OTG_SPEED_HIGH);
    if (root_speed == USB_OTG_SPEED_LOW) {
        dev_speed = HCD_SPEED_LOW;
    } else {
        dev_speed = HCD_SPEED_FULL;
    }

#if 1
    const uint8_t GET_DESCRIPTOR = 0x06;
    uint8_t setup[8] = {0x80, GET_DESCRIPTOR, 0x00, 0x01, 0x00, 0x00, 8, 0x00};
    uint8_t buf[18];
    ctrl_recv(setup, buf, 8);
    USB_TRACE_VIEW();
    USB_DBG_HEX(buf, 8);
    dev_mps = buf[7];
    USB_DBG("dev_mps=%d", dev_mps);
    USB_TEST_ASSERT(dev_mps >= 8);
#endif

    const uint8_t SET_ADDRESS = 0x05;
    const uint8_t new_dev_address = 1;
    uint8_t setup_set_address[8] = {0x00, SET_ADDRESS, new_dev_address, 0x00, 0x00, 0x00, 0x00, 0x00};
    ctrl_send(setup_set_address);
    dev_address = new_dev_address;
    delay_ms(100);

#if 1
    uint8_t desc[18];
    uint8_t setup_get_desc[8] = {0x80, GET_DESCRIPTOR, 0x00, 0x01, 0x00, 0x00, sizeof(desc), 0x00};
    ctrl_recv(setup_get_desc, desc, sizeof(desc));
    USB_DBG_HEX(desc, sizeof(desc));
#endif

#if 1
    uint8_t temp[4];
    uint8_t setup_get_desc2[8] = {0x80, GET_DESCRIPTOR, 0x00, 0x02, 0x00, 0x00, sizeof(temp), 0x00};
    ctrl_recv(setup_get_desc2, temp, sizeof(temp));
    USB_DBG_HEX(temp, sizeof(temp));
    int totalLength = temp[2]|temp[3]<<8;
    USB_DBG("totalLength=%d", totalLength);
    uint8_t* desc2 = new uint8_t[totalLength];
    memcpy(setup_get_desc2+6, temp+2, 2);

    USB_TRACE_CLEAR();
    ctrl_recv(setup_get_desc2, desc2, totalLength);

    USB_TRACE_VIEW();
    USB_DBG_HEX(desc2, totalLength);
    for(int pos = 0; pos < totalLength;) {
        int len = desc2[pos];
        USB_DBG_HEX(desc2+pos, len);
        pos += len;
    }
#endif

    const uint8_t SET_CONFIGURATION = 0x09;
    uint8_t setup_set_config[8] = {0x00, SET_CONFIGURATION, 1, 0x00, 0x00, 0x00, 0x00, 0x00};
    ctrl_send(setup_set_config);

#if 1
    USB_TRACE_CLEAR();
    uint8_t data[4];

    for(int cnt = 0; cnt < 100;) {
        if(intr_recvNB(data, sizeof(data))) {
            USB_DBG_HEX(data, sizeof(data));
            cnt++;
            int r1 = HAL_HCD_HC_GetURBState(&hhcd_USB_OTG_FS, 2);
            USB_TRACE1(r1);
            int r2 = HAL_HCD_HC_GetState(&hhcd_USB_OTG_FS, 2);
            USB_TRACE1(r2);
        }
        delay_ms(10);
    }
    //USB_TRACE_VIEW(RawSerial, pc);

#endif
    
    while(1) {
        led1 = !led1;
        delay_ms(200);
    }
}

#endif
