#pragma once
#include "mbed.h"

#if 1

#define USB_TRACE() myt.trace(__func__, __LINE__)
#define USB_TRACE1(A) myt.trace1(__func__, __LINE__, A)
#define USB_TRACE_CLEAR() myt.clear();

#else

#define USB_TRACE() while(0)
#define USB_TRACE1(A) while(0)
#define USB_TRACE_VIEW() while(0)
#define USB_TRACE_CLEAR() while(0)

#endif

class myTrace {
public:
    myTrace();
    void trace(const char* func, int line);
    void trace1(const char* func, int line, int value);
    template<class SERIAL_T>
    void view(SERIAL_T& pc) {
        int rpos = wpos - TRACE_BUF_SIZE;
        rpos = rpos < 0 ? 0 : rpos;
        for(int i = rpos; i < wpos; i++) {
            pc.printf("[%s@%d] %d\n",
                buf[i%TRACE_BUF_SIZE].func,
                buf[i%TRACE_BUF_SIZE].line, 
                buf[i%TRACE_BUF_SIZE].value);
       }
       clear();
    }
    void clear() { wpos = 0; }

private:
    int wpos;
    static const int TRACE_BUF_SIZE = 256;
    struct {
        const char* func;
        uint16_t line;
        int value;
    } buf[TRACE_BUF_SIZE];
};

extern myTrace myt;


