#ifdef TEST_CTL1
#include "mbed.h"
#include "mytrace.h"
#include "mydebug.h"
#include "USBHALHost.h"
#include <algorithm>

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);

#define USB_DBG(...) do{pc.printf("[%s@%d] ",__PRETTY_FUNCTION__,__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define USB_INFO(...) do{pc.printf(__VA_ARGS__);pc.puts("\n");}while(0);
#define USB_TEST_ASSERT(A) while(!(A)){pc.printf("\n\n%s@%d %s ASSERT!\n\n",__PRETTY_FUNCTION__,__LINE__,#A);exit(1);};
#define USB_DBG_HEX(A,B) debug_hex<RawSerial>(pc, A,B)

class USBHost : public USBHALHost {
public:
    static USBHost* getHostInst();

    int ControlRead(USBDeviceConnected* dev, SETUP_PACKET* setup, uint8_t* data, int size);
    int ControlWrite(USBDeviceConnected* dev, SETUP_PACKET* setup, uint8_t* data = NULL, int size = 0);

private:
    USBHost(){}
    static USBHost* inst;
    virtual bool addDevice(USBDeviceConnected* parent, int port, bool lowSpeed);
};

USBHost* USBHost::inst = NULL;
USBHost* USBHost::getHostInst() {
    if (inst == NULL) {
        inst = new USBHost();
        inst->init();
    }
    return inst;
}

USBDeviceConnected::USBDeviceConnected(){}
USBDeviceConnected* dev;


/* virtual */ bool USBHost::addDevice(USBDeviceConnected* parent, int port, bool lowSpeed) {
    USB_DBG("lowSpeed=%d", lowSpeed);
    dev = new USBDeviceConnected;
    dev->setAddress(0);
    dev->setSpeed(lowSpeed);
    USBEndpoint* ep_ctl = new USBEndpoint(dev);
    dev->setEpCtl(ep_ctl);
    return true;
}

int USBHost::ControlRead(USBDeviceConnected* dev, SETUP_PACKET* setup, uint8_t* data, int size) {
    USB_TEST_ASSERT(dev);
    USBEndpoint* ep = dev->getEpCtl();
    USB_TEST_ASSERT(ep);
    setAddr(dev->getAddress(), dev->getSpeed());
    token_setup(ep, setup, size); // setup stage
    if (LastStatus != ACK) {
        USB_DBG("setup %02x", LastStatus);
        return -1;
    }
    int read_len = 0;
    while(read_len < size) {
        int size2 = std::min(size-read_len, ep->getSize());
        int result = token_in(ep, data+read_len, size2);
        //USB_DBG("token_in result=%d %02x", result, LastStatus);
        if (result < 0) {
            USB_DBG("token_in %d/%d %02x", read_len, size, LastStatus);
            return result;
        }
        read_len += result;
        if (result < ep->getSize()) {
            break;
        }
    }    
    ep->setData01(DATA1);
    int result = token_out(ep); // status stage
    if (result < 0) {
        USB_DBG("status token_out %02x", LastStatus);
        if (LastStatus == STALL) {
            ep->setLengthTransferred(read_len);
            return read_len;
        }
        return result;
    }
    ep->setLengthTransferred(read_len);
    return read_len;
}

int USBHost::ControlWrite(USBDeviceConnected* dev, SETUP_PACKET* setup, uint8_t* data, int size) {
    USB_TEST_ASSERT(dev);
    USBEndpoint* ep = dev->getEpCtl();
    USB_TEST_ASSERT(ep);
    setAddr(dev->getAddress(), dev->getSpeed());
    token_setup(ep, setup, size); // setup stage
    if (LastStatus != ACK) {
        USB_DBG("setup %02x", LastStatus);
        return -1;
    }
    int write_len = 0;
    if (data != NULL) {
        write_len = token_out(ep, data, size);
        if (write_len < 0) {
            return -1;
        }
    }
    ep->setData01(DATA1);
    int result = token_in(ep); // status stage
    if (result < 0) {
        USB_DBG("result=%d %02x", result, LastStatus);
        //return result;
    }
    ep->setLengthTransferred(write_len);
    return write_len;
}


int main() {
    pc.baud(115200);
    USB_DBG("%s", __FILE__);

    USBHost* host = USBHost::getHostInst();
#if 1
    SETUP_PACKET setup = {0x80, 0x06, 1<<8, 0};
    uint8_t buf[8];
    int result = host->ControlRead(dev, &setup, buf, sizeof(buf));
    USB_DBG("result=%d", result);
    USB_DBG_HEX(buf, result);
#endif

#if 1
    SETUP_PACKET setup1 = {0x80, 0x06, 1<<8, 0};
    uint8_t buf1[64];
    int result1 = host->ControlRead(dev, &setup1, buf1, sizeof(buf1));
    USB_DBG("result1=%d", result1);
    USB_DBG_HEX(buf1, result1);
#endif
    
#if 1
    SETUP_PACKET setup2 = {0x80, 0x06, 2<<8, 0};
    uint8_t buf2[64];
    int result2 = host->ControlRead(dev, &setup2, buf2, sizeof(buf2));
    USB_DBG("result2=%d", result2);
    USB_DBG_HEX(buf2, result2);
#endif
    
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}

#endif

