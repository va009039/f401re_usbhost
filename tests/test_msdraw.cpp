#ifdef TEST_MSDRAW
#include "mbed.h"
#include "mytest.h"

#include "USBHostMSDRaw.h"

DigitalOut led1(LED1);
USBHostMSDRaw* msdraw = NULL;
uint8_t buf[512];

static void debug_hex(uint8_t* buf, int size) {
    for(int i = 0; i < size; i++) {
        debug("%02x ", buf[i]);
    }
    debug("\n");
}

TEST(MSDRaw, test0) {
    msdraw = new USBHostMSDRaw("usb");
    ASSERT_TRUE(msdraw->connect());
}

TEST(MSDRaw, test1) {
    memset(buf, 0xaa, sizeof(buf));
    ASSERT_TRUE(msdraw->disk_read(buf, 0) == 0);
    ASSERT_TRUE(buf[0] != 0xaa);
}

TEST(MSDRaw, test2) {
    for(int s = 0; s < 8; s++) {
        memset(buf, 0x55, sizeof(buf));
        ASSERT_TRUE(msdraw->disk_read(buf, s) == 0);
        ASSERT_TRUE(buf[0] != 0x55);

        debug_hex(buf, 16);
    }
}

uint8_t backup_data[512];
void backup(int s) {
    msdraw->disk_read(backup_data, s);
}

void restore(int s) {
    msdraw->disk_write(backup_data, s);
}

void build_data(uint8_t* buf, int key = 0) {
    for(int i = 0; i < 512; i++) {
        buf[i] = i + key;
    }
}

TEST(MSDRaw, test3) {
    for(int s = 0; s < 8; s++) {
        backup(s);
        uint8_t data[512];
        build_data(data, s);
        TEST_PRINT("write sector=%d", s);
        ASSERT_TRUE(msdraw->disk_write(data, s) == 0);
        memset(buf, 0xaa, sizeof(buf));
        TEST_PRINT("read sector=%d", s);
        ASSERT_TRUE(msdraw->disk_read(buf, s) == 0);
        debug_hex(buf, 16);
        ASSERT_TRUE(memcmp(buf, data, 512) == 0);
        restore(s);
    }
}

int main() {
    //pc.baud(115200);
    printf("%s", __FILE__);

    RUN_ALL_TESTS();

    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}

#endif


