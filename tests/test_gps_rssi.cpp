#ifdef TEST_GPS_RSSI
#include "USBHostGPS.h"
#include "USBHostRSSI.h"
#include "mytest.h"

DigitalOut led1(LED1);

USBHostGPS* gps = NULL;
USBHostRSSI* bt = NULL;

__IO bool gps_done = false;

void callback_gps(char* buf, int size) {
    static int n = 0;
    static int pos = 0;
    static char line[64];
    
    static uint8_t cksum;
    for(int i = 0; i < size; i++) {
        char c = buf[i];
        if (pos < sizeof(line)-1) {
            line[pos++] = c;
        } else {
            line[pos] = '\0';
            TEST_PRINT("%s", line);
            pos = 0;
        }
        if (c == '$') {
            cksum = 0;
        } else if (c == '*') {
            TEST_PRINT("[checksum=%02X]", cksum);
            n++;

        } else {
            cksum ^= c;
        }
    }
    if (n > 5) {
        gps_done = true;
    }
}

__IO bool bt_done = false;

void callback_bt(inquiry_with_rssi_info* info) {
    static int n = 0;
    char buf[18];
    info->bdaddr.str(buf, sizeof(buf));
    TEST_PRINT("%d %s %d", n, buf, info->rssi);
    if (++n > 10) {
        bt_done = true;
    }
}

TEST(RSSI,test0) {
    bt = new USBHostRSSI;
    ASSERT_TRUE(bt->connect());
    bt->attachEvent(callback_bt);
}

TEST(GPS,test0) {
    gps = new USBHostGPS(4800);
    ASSERT_TRUE(gps->connect());
    gps->attachEventRaw(callback_gps);
}

TEST(RSSI,test1) {
    TEST_PRINT("wait bt");
    ASSERT_TRUE(bt);
    while(!bt_done) {
        USBHost::poll();
    }
    ASSERT_TRUE(bt_done);
}

TEST(GPS,test1) {
    TEST_PRINT("wait gps");
    ASSERT_TRUE(gps);
    while(!gps_done) {
        USBHost::poll();
    }
    ASSERT_TRUE(gps_done);
}

int main() {
    TEST_PRINT("%s", __FILE__);

    RUN_ALL_TESTS();

    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}

#endif // TEST_GPS_RSSI


