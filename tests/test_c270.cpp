#ifdef TEST_C270
#include "USBHostCam.h"
#include "mytest.h"

RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);
uint8_t image_buf[1024*8];

void image_hex(uint8_t* data, int size) {
    for(int i = 0; i < size; i++) {
        debug("%02x ", data[i]);
        if (i % 32 == 15) {
            debug("\n");
        }
    }
    debug("\n\n");
}

USBHostCam* cam = NULL;

TEST(USBHostCam,test0) {
    // Logitech C270
    cam = new USBHostCam(_320x240);
    ASSERT_TRUE(cam->connect());
}

TEST(USBHostCam,test1) {
    ASSERT_TRUE(cam);
    int size = cam->readJPEG(image_buf, sizeof(image_buf));
    ASSERT_TRUE(size > 0);
    image_hex(image_buf, 64);
}

TEST(USBHostCam,test2) {
    ASSERT_TRUE(cam);
    int size = cam->readJPEG(image_buf, sizeof(image_buf));
    ASSERT_TRUE(size > 0);
    image_hex(image_buf, 64);
}

TEST(USBHostCam,test3) {
    ASSERT_TRUE(cam);
    for(int i = 0; i < 10; i++) {
        int size = cam->readJPEG(image_buf, sizeof(image_buf));
        TEST_PRINT("shot=%d, size=%d", i, size);
        ASSERT_TRUE(size > 0);
    }
}

int main() {
    //pc.baud(115200);
    printf("%s", __FILE__);

    RUN_ALL_TESTS();

    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}

#endif // TEST_C270

