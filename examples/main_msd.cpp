#ifdef MAIN_MSD
// Simple USBHost MSD for Nucleo F401RE test program
#include "mbed.h"
#include "USBHostMSD.h"

DigitalOut led1(LED1);
#define LED_OFF 1
#define LED_ON  0

int main() {
    USBHostMSD msd("usb");
    if (!msd.connect()) {
        error("USB Flash drive not found.\n");
    }
    FILE* fp = fopen("/usb/test3.txt", "r");
    printf("file read test fp=%p\n", fp);
    if (fp) {
        while(1) {
            int c = fgetc(fp);
            if (c == EOF) {
                break;
            }
            //printf("%02x %c\n", c, c>0x20 ? c : '.');
            printf("%c", c);
        }
        fclose(fp);
    }
    fp = fopen("/usb/test4.txt", "a");
    printf("file write test fp=%p\n", fp);
    if (fp) {
        for(int i = 0; i < 20; i++) {
            fprintf(fp, " %d", i);
        }
        fprintf(fp, "\n");
        fclose(fp);
    }

    //msd.report->print_errstat();
    
    fp = fopen("/usb/test4.txt", "r");
    printf("file read fp=%p\n", fp);
    if (fp) {
        int n = 0;
        while(1) {
            int c = fgetc(fp);
            if (c == EOF) {
                break;
            }
            printf("%c", c);
            n++;
        }
        printf("%d bytes\n", n);
        fclose(fp);
    }
    led1 = LED_OFF;
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}
#endif


