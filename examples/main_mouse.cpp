#ifdef MAIN_MOUSE
// Simple USBHost Mouse for Nucleo F401RE test program

#include "USBHostMouse.h"

DigitalOut led1(LED1);
#define LED_OFF 1
#define LED_ON  0

void callback(uint8_t buttons) {
    led1 = (buttons&1) ? LED_ON : LED_OFF; // button on/off
    //led2 = (buttons&2) ? LED_ON : LED_OFF;
    printf("%02x\n", buttons);
}

int main() {
    USBHostMouse mouse;
    if (!mouse.connect()) {
        error("USB mouse not found.\n");
    }
    mouse.attachButtonEvent(callback);
    while(1) {
        USBHost::poll();
    }
}
#endif // MAIN_MOUSE

